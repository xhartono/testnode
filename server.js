const express = require('express');
const cors = require('cors');
const app = express();

// Basic Configuration
const port = process.env.PORT || 5000;
const hostname = process.env.HOSTNAME || '127.0.0.1';

app.use(express.static(__dirname + "/public"));
app.use(cors());
app.use(function (req,res,next){
  console.log(req.method + " " + req.path + " - " + req.ip);
  next();
})

app.get('/', function(req, res) {
  res.send("CICD");
});

app.get('/port', function(req, res) {
  res.send("The port:" + port);
});

exports.getPort = () => {
    return port;
}

var sJS = app.listen(port, hostname , () => {
  console.log(`Example app listening at http://${hostname}:${port}`)
})
