var expect  = require('chai').expect;
var request = require('request');
var sJS = require('../server.js');

describe('Test running: ', ()=> {
    it('Test call: Should return - Hello NodeJS Test with Gitlab and testing Gitlab CICD', (done) => {
        request('http://localhost:5000' , (error, response, body)=> {
            expect(body).to.equal('CICD');
            sJS.close(done);
        });
    });
})